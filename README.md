# storagetools
Profiling storage memory in Max-Planck Institute For Brain Research, Frankfurt am Main

## installation

```
git clone https://gitlab.mpcdf.mpg.de/mpibr/scic/storagetools.git
cd storagetools
mkdir build
cd ./build
cmake ..
make
./storagetools --help
```

## tools

```
storagetools
a toolset to profile file statistics
version 0.0.1

usage: storagetools <subcommand> [-h|--help -v|--version -c|--contact]

[subcommands]
    stats      traverse directory tree and print out file information
    summary    read file information from STDIN and summarize counts and size

[options]
    -h, --help     print this help message
    -v, --version  what major.minor.build version of ribotools is used
    -c, --contact  feature requests, bugs, mailing lists, etc.
```

## run

```
$ find /path/of/interest -type f | ./storagetools stats | gzip > /path/of/interest/dataStats.txt.gz
$ zcat /path/of/interest/dataStats.txt.gz | ./storagetools summary > /path/of/interest/dataSummary.txt
```
