cmake_minimum_required(VERSION 3.5)

project(storagetools LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(storagetools main.cpp main_cstats.cpp main_stats.cpp main_summary.cpp)

# Link OpenSSL
if (APPLE)
    set(OPENSSL_ROOT_DIR /usr/local/Cellar/openssl@1.1/1.1.1d/)
    find_package(openssl REQUIRED)
    message(STATUS "ROOTDIR " ${OPENSSL_ROOT_DIR})
    message(STATUS "FOUND " ${OPENSSL_FOUND})

    if (OPENSSL_FOUND)
        message(STATUS "INCLUDE " ${OPENSSL_INCLUDE_DIR})
        message(STATUS "LIBS::SSL " ${OPENSSL_SSL_LIBRARIES})
        message(STATUS "LIBS::CRYPTO " ${OPENSSL_CRYPTO_LIBRARIES})
        include_directories(${OPENSSL_INCLUDE_DIR})
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -L/usr/local/Cellar/openssl@1.1/1.1.1d/lib -lssl -lcrypto")
    endif (OPENSSL_FOUND)

elseif (UNIX)
    find_package(OpenSSL REQUIRED)
    target_link_libraries(${PROJECT_NAME} OpenSSL::SSL)
    target_link_libraries(${PROJECT_NAME} OpenSSL::Crypto)
endif()





