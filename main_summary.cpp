#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>
#include <algorithm>

typedef struct {
    uint64_t count;
    uint64_t fileSize;
    uint64_t diskSize;
} SummaryValue;


template<typename T>
void addToMap(std::unordered_map<T, SummaryValue> &map, T key, uint64_t file_size, uint64_t file_disk)
{
    typename std::unordered_map<T, SummaryValue>::iterator it = map.find(key);
    if (it == map.end()) {
        SummaryValue value = {1, file_size, file_disk};
        map[key] = value;
    }
    else {
        it->second.count++;
        it->second.fileSize += file_size;
        it->second.diskSize += file_disk;
    }
}


template<typename T>
void printMap(const std::unordered_map<T, SummaryValue> &map)
{
    for(auto it : map)
        std::cout << it.first << "\t" << it.second.count << "\t" << it.second.fileSize << "\t" << it.second.diskSize << std::endl;
}


std::string str_tolower(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return std::tolower(c); } // correct
                  );
    return s;
}

int main_summary(const int argc, const char *argv[])
{
    std::unordered_map<int, SummaryValue> mapYear;
    std::unordered_map<std::string, SummaryValue> mapUser;
    std::unordered_map<std::string, SummaryValue> mapType;

    for (std::string line; std::getline(std::cin, line);) {

        std::istringstream iss(line);
        std::string user_name;
        std::string file_extension;
        int file_myear;
        uint64_t file_size;
        uint64_t file_disk;
        uint64_t disk_block;

        iss >> user_name >> file_extension >> file_myear >> file_size >> file_disk >> disk_block;

        addToMap<int>(mapYear, file_myear, file_size, file_disk);
        addToMap<std::string>(mapUser, user_name, file_size, file_disk);
        addToMap<std::string>(mapType, str_tolower(file_extension), file_size, file_disk);
    }

    // print summary
    std::cout << "# modified year" << std::endl;
    printMap<int>(mapYear);
    std::cout << "# user name" << std::endl;
    printMap<std::string>(mapUser);
    std::cout << "# file extension" << std::endl;
    printMap<std::string>(mapType);

    return 0;
}
