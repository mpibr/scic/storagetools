#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <pwd.h>
#include <time.h>
#include <algorithm>
#include <openssl/md5.h>
#include <iomanip>

// activate usage of stat64 for files bigger than 32bit offset
#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS == 64
#endif


std::string getFileExtension(std::string filePath)
{
    std::size_t posDot = filePath.rfind('.');
    std::size_t posSlash = filePath.rfind('/');

    if (posDot != std::string::npos && posDot > posSlash)
        return filePath.substr(posDot + 1);

    return "binary";
}


const std::string getFileChecksum(const std::string &fileName, int64_t offset)
{
    const std::size_t buffer_size = 1024;
    char buffer[buffer_size];
    unsigned char digest[MD5_DIGEST_LENGTH];
    std::ifstream ifs(fileName, std::ifstream::binary);

    MD5_CTX ctx;
    MD5_Init(&ctx);
    ifs.seekg(-offset, std::ios_base::end);
    while (ifs.good()) {
        ifs.read(buffer, buffer_size);
        MD5_Update(&ctx, buffer, static_cast<std::size_t>(ifs.gcount()));
    }
    ifs.close();

    int result = MD5_Final(digest, &ctx);
    if (result == 0)
        return "unknown";

    // convert to hex string
    std::stringstream ss;
    ss << std::hex << std::nouppercase << std::setfill('0');
    for (unsigned char us : digest)
        ss << std::setw(2) << static_cast<int>(us);

    return ss.str();
}


void fileStats(bool flagChecksum)
{
    for (std::string fileName; std::getline(std::cin, fileName);) {

        // file stats
        struct stat info;
        if (stat(fileName.c_str(), &info) != 0)
            continue;

        // get file size variables
        const int64_t file_size = info.st_size;
        const int64_t block_size = info.st_blksize;
        const int64_t block_count = info.st_blocks;

        // get last modified year
        struct tm mtime;
        localtime_r(&(info.st_mtime), &mtime);
        const int year = mtime.tm_year + 1900;

        // get user name
        struct passwd *pws;
        pws = getpwuid(info.st_uid);
        const char* name;
        if (pws == nullptr)
            name = "unknown";
        else
            name = pws->pw_name;

        // file type
        std::string file_type = getFileExtension(fileName);
        if (file_type.empty())
            file_type = "binary";

        // print result
        std::cout << fileName;
        std::cout << "\t" << name;
        std::cout << "\t" << file_type;
        std::cout << "\t" << year;
        std::cout << "\t" << file_size;
        std::cout << "\t" << (block_count * 512);
        std::cout << "\t" << block_size;

        // checksum
        if (flagChecksum) {
            const int64_t offset = 1024;
            std::cout << "\t" << getFileChecksum(fileName, std::min(offset, file_size));
        }

        std::cout << std::endl;
    }

}


int printUsageStats()
{
    std::cerr << "storagetools::stats" << std::endl;
    std::cerr << "traverse directory tree and prints out file information" << std::endl;
    std::cerr << std::endl;
    std::cerr << "usage: storagetools stats [options] <stdin>" << std::endl;
    std::cerr << std::endl;
    std::cerr << "[options]" << std::endl;
    std::cerr << "    -c, --checksum    caclculate MD5 checksum" << std::endl;
    std::cerr << "    -h, --help        print this help message" << std::endl;
    std::cerr << std::endl;

    return 0;
}


int main_stats(const int argc, const char *argv[])
{

    bool flagChecksum = false;

    if (argc > 1) {

        for (int i = 1; i < argc; i++) {
            const std::string argoption = std::string(argv[i]);
            if (argoption == "-h" || argoption == "--help") return printUsageStats();

            else if (argoption == "-c" || argoption == "--checksum") flagChecksum = true;

            else {
                std::cerr << "storagetools::stats::error, unknown option " << argoption << std::endl;
                return printUsageStats();
            }

        }

    }

    fileStats(flagChecksum);

    return 0;
}

