/*
 * adapted from
 * https://stackoverflow.com/questions/8436841/how-to-recursively-list-directories-in-c-on-linux
 *
 * credits go to:
 * https://stackoverflow.com/users/1475978/nominal-animal
 */

#include <iostream>
#include <cstring>
#include <errno.h>
#include <ftw.h>
#include <time.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>


/* We want POSIX.1-2008 + XSI, i.e. SuSv4, features */
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif

/* Added on 2017-06-25:
   If the C library can support 64-bit file sizes
   and offsets, using the standard names,
   these defines tell the C library to do so. */
#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif

/* POSIX.1 says each process has at least 20 file descriptors.
 * Three of those belong to the standard streams.
 * Here, we use a conservative estimate of 15 available;
 * assuming we use at most two for other uses in this program,
 * we should never run into any problems.
 * Most trees are shallower than that, so it is efficient.
 * Deeper trees are traversed fine, just a bit slower.
 * (Linux allows typically hundreds to thousands of open files,
 *  so you'll probably never see any issues even if you used
 *  a much higher value, say a couple of hundred, but
 *  15 is a safe, reasonable value.)
*/
#ifndef USE_FDS
#define USE_FDS 15
#endif


const char *get_file_extension(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "binary";
    return dot + 1;
}

int print_entry(const char *filepath,
               const struct stat *info,
               const int typeflag,
               struct FTW *pathinfo)
{
    // ignore directories and softlinks
    if (!S_ISREG(info->st_mode))
        return 0;

    // get file size variables
    const int64_t file_size = info->st_size;
    const int64_t block_size = info->st_blksize;
    const int64_t block_count = info->st_blocks;

    // get last modification year
    struct tm mtime;
    localtime_r(&(info->st_mtime), &mtime);
    const int year = mtime.tm_year + 1900;

    // get user name
    struct passwd *pws;
    const char *name;
    pws = getpwuid(info->st_uid);
    if (pws == nullptr)
        name = "unknown";
    else
        name = pws->pw_name;

    // file name
    const char *const ext = get_file_extension(filepath + pathinfo->base);
    printf("%s\t%s\t%d\t%lld\t%lld\t%lld\n", name, ext, year, file_size, block_count * 512, block_size);

    return 0;
}


int walk_directory_tree(const char *const dir_path)
{
    int result;

    // invalid directory
    if (dir_path == nullptr || *dir_path == '\0')
        return errno = EINVAL;

    result = nftw(dir_path, print_entry, USE_FDS, FTW_MOUNT | FTW_PHYS);
    if (result >= 0)
        errno = result;

    return errno;
}



int main_cstats(const int argc, const char *argv[])
{
    if (argc < 2) {

        if (walk_directory_tree(".")) {
            fprintf(stderr, "%s.\n", std::strerror(errno));
            return EXIT_FAILURE;
        }

    }
    else {

        for (int arg = 1; arg < argc; arg++) {
            if (walk_directory_tree(argv[arg])) {
                fprintf(stderr, "%s.\n", std::strerror(errno));
                return EXIT_FAILURE;
            }
        }

    }

    return EXIT_SUCCESS;
}
