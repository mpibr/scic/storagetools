#include <iostream>

#define VERSION "0.0.1"

int main_cstats(const int argc, const char *argv[]);
int main_stats(const int argc, const char *argv[]);
int main_summary(const int argc, const char *argv[]);

int printUsage();
int printVersion();
int printContact();

int main(const int argc, const char *argv[])
{
    // make sure sub command is present
    if (argc < 2)
        return printUsage();

    // parse on subcommand
    const std::string subcommand = std::string(argv[1]);

    if (subcommand == "-h" || subcommand == "--help") return printUsage();

    else if (subcommand == "-v" || subcommand == "--version") return printVersion();

    else if (subcommand == "-c" || subcommand == "--contact") return printContact();

    else if (subcommand == "cstats") return main_cstats(argc - 1, argv + 1);

    else if (subcommand == "stats") return main_stats(argc - 1, argv + 1);

    else if (subcommand == "summary") return main_summary(argc - 1, argv + 1);

    else {
        std::cerr << "storagetools::error, unknown subcommand " << subcommand << std::endl;
        return printUsage();
    }
}


int printVersion()
{
    std::cout << "version " << VERSION << std::endl;
    return 0;
}


int printContact()
{
    std::cerr << "Scientific Computing Facility" << std::endl;
    std::cerr << "Max-Planck Institute For Brain Research" << std::endl;
    std::cerr << "Frankfurt am Main, Germany" << std::endl;
    std::cerr << "source code: https://gitlab.mpcdf.mpg.de/mpibr/scic/storagetools" << std::endl;
    std::cerr << "bug reports: sciclist@brain.mpg.de" << std::endl;
    std::cerr << "author: georgi a. tushev" << std::endl;
    return 0;
}


int printUsage()
{
    std::cerr << "storagetools" << std::endl;
    std::cerr << "a toolset to profile file statistics" << std::endl;
    printVersion();
    std::cerr << std::endl;
    std::cerr << "usage: storagetools <subcommand> [-h|--help -v|--version -c|--contact]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "[subcommands]" << std::endl;
    std::cerr << "    stats      traverse directory tree and prints out file information" << std::endl;
    std::cerr << "    summary    reads file information from STDIN and summarizes counts and size" << std::endl;
    std::cerr << std::endl;
    std::cerr << "[options]" << std::endl;
    std::cerr << "    -h, --help     print this help message" << std::endl;
    std::cerr << "    -v, --version  what major.minor.build version of ribotools is used" << std::endl;
    std::cerr << "    -c, --contact  feature requests, bugs, mailing lists, etc." << std::endl;
    std::cerr << std::endl;

    return 0;
}

